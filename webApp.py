#!/usr/bin/env python3

#
# webApp class
# Root for hierarchy of classes implementing web applications
#
# Copyright Jesus M. Gonzalez-Barahona 2009-2020
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
#

import socket
import urllib.parse
class WebApp:
    """Root of a hierarchy of classes implementing web applications

    This class does almost nothing. Usually, new classes will
    inherit from it, and by redefining "parse" and "process" methods
    will implement the logic of a web application in particular.
    """

    def parse(self, request):
        received = request.decode()
        method = received.split(' ')[0]
        if method == 'POST':
            resource = received.split('\r\n\r\n')[1]
            resource = urllib.parse.unquote(resource)
        else:
            resource = received.split(' ')[1][0:]
            resource = urllib.parse.unquote(resource)
        return {'resource': resource, 'method': method}

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello World!</h1></body></html>"
        return http, html
    def __init__ (self, host, port):
        """Initialize the web application."""
        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))

        # Queue a maximum of 5 TCP connection requests
        self.socket.listen(5)


    def accept_clients(self):
        while True:
            (client_socket, client_address) = self.socket.accept()
            request = client_socket.recv(1024)
            if not request:
                continue
            parsed_request = self.parse(request)
            http, html = self.process(parsed_request)
            if http == "HTTP/1.1 301 Moved Permanently\r\n":
                response = http + html
                client_socket.send(response.encode('ascii'))
            else:
                response = f"HTTP/1.1 200 OK\r\n\r\n{html}"
                client_socket.sendall(response.encode('utf-8'))
            client_socket.close()


